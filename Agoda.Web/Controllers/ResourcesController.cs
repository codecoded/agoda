﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Agoda.Messaging;
using Agoda.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;

namespace Agoda.Web.Controllers
{
    [Route("api/[controller]")]
    public class ResourcesController : Controller
    {
        readonly IMessageProducer _broker;
        readonly IDatabase _database;

        public ResourcesController(IMessageProducer broker, IDatabase database)
        {
            _database = database;
            _broker = broker;
        }

        [HttpGet("{id}")]
        public async Task<string> Get(string id)
        {
            var status = await _database.StringGetAsync(id);

            if (!string.IsNullOrEmpty(status))
                return status;

            return $"{id} could not be found";
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody]BatchResourceRequest batchRequest)
        {
            if ((batchRequest?.Requests?.Count).GetValueOrDefault() == 0 )
                return new BadRequestResult();

            var topicName = "batch_resource_request_queued";
            var response = await _broker.SendMessage(batchRequest, topicName);

            await _database.StringGetSetAsync(response.Id.ToString(), response.Status.ToString());
            if (response.Status == Messaging.Models.MessageStates.Queued)
                return Accepted(response.Id);

            return new StatusCodeResult(500);
        }
    }
}
