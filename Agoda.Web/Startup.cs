﻿
using Agoda.Messaging;
using Agoda.Messaging.Brokers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using StackExchange.Redis;

namespace Agoda.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSingleton<IMessageProducer, KafkaProducer>(serviceProvider =>
                {
                    return new KafkaProducer(Configuration["Brokers:Kafka:List"]);
                });

            if (!int.TryParse(Configuration["Brokers:Redis:Database"], out int redisDatabase))
                redisDatabase = 0;

            var connectionMultiplexer = ConnectionMultiplexer.Connect(Configuration["Brokers:Redis:Connection"]);
            var database = connectionMultiplexer.GetDatabase(redisDatabase);
            services.AddScoped(_ => database);

            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
