﻿using System;
using Agoda.Consumer.Services;
using Agoda.Messaging.Brokers;
using StackExchange.Redis;

namespace Agoda.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            string topicName = "batch_resource_request_queued";
            string brokerList = args?.Length > 1 ? args[0] : "kafkaserver:9092";
            string redis = args?.Length > 1 ? args[1] : "rediscache";

            Console.WriteLine("Usage: Agoda.Consumer <kafka endoint> <redis endopint>. Defaults are 'kafkaserver:9092' and 'rediscache'. Add to Hosts file if required.");

            Console.WriteLine($"Consuming messages from {brokerList} (args[0]), topic {topicName}. Using redis {redis} (args[1])");

            var connectionMultiplexer = ConnectionMultiplexer.Connect(redis);
            var database = connectionMultiplexer.GetDatabase(0);


            using (var kafkaConsumer = new KafkaConsumer(brokerList))
            {
                using (var kafkaProducer = new KafkaProducer(brokerList))
                {
                    // could abstract to an interface and inject into KakfaMessageConsumer constructor
                    var messageHandler = new MessageHandler(database, kafkaProducer);

                    kafkaConsumer.Consumer.OnMessage += messageHandler.OnMessage;
                    kafkaConsumer.Consumer.OnError += (_, error) => Console.WriteLine($"Error: {error}");
                    kafkaConsumer.Consumer.OnConsumeError += (_, msg) => Console.WriteLine($"Consume error ({msg.TopicPartitionOffset}): {msg.Error}");

                    kafkaConsumer.ConsumeMessages(topicName);
                }
            }
        }
    }
}
