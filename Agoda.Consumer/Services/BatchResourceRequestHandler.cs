﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Agoda.Shared.Models;
using StackExchange.Redis;

namespace Agoda.Consumer.Services
{
    public class BatchResourceRequestHandler
    {
        readonly BatchResourceRequest _batchResourceRequest;
        readonly IDatabase _database;
        public string OutDirectory { get; set; }

        public BatchResourceRequestHandler(BatchResourceRequest batchResourceRequest, IDatabase database, string outputDirectory ="\\tmp\\downloads")
        {


            _batchResourceRequest = batchResourceRequest;
            _database = database;
            OutDirectory = outputDirectory;

            if (!Directory.Exists(OutDirectory))
                Directory.CreateDirectory(OutDirectory);
        }

        public void ProcessBatch()
        {
            Console.WriteLine($"Processing Batch {_batchResourceRequest.Id}");

            UpdateBatchStatus();

            if (_batchResourceRequest.Status != ResourceRequestStates.InProgress)
                return;

            var successResults = _batchResourceRequest.Requests.Select(ProcessRequest).ToArray();
            Task.WaitAll(successResults);

            if (_batchResourceRequest.AllRequestsCompleted)
                SetAsCompleted();
            else
                SetAsFailed();
        }

        async Task ProcessRequest(ResourceRequest request)
        {
            request.State = ResourceRequestStates.InProgress;
            var resourceDownloader = new ResourceRequestDownloader(request);
            request.State = await resourceDownloader.Download(OutDirectory) ? ResourceRequestStates.Completed : ResourceRequestStates.Failed;
        }

        // Safety check to make sure message is in correct state
        // https://en.wikipedia.org/wiki/State_pattern  - can work on a cleaner design 
        // e.g. If all is good, call NextState() rather than explicit
        void UpdateBatchStatus()
        {
            if (IsInQueuedState())
                SetAsInProgess();
            else
                SetAsFailed();
        }

        bool IsInQueuedState()
        {
            var cachedStatus = _database.StringGet(_batchResourceRequest.Id.ToString());

            if (string.IsNullOrEmpty(cachedStatus) || cachedStatus.IsNullOrEmpty)
                return false;


            if (Enum.TryParse(cachedStatus, out ResourceRequestStates state))
            {
                _batchResourceRequest.SetState(state);
                return state == ResourceRequestStates.Queued;
            }

            // Log / throw excpetion? Something gone very wrong if we are here
            return false;
        }

        public bool SetAsCompleted()
        {
            return UpdateStatus(ResourceRequestStates.Completed);
        }

        public bool SetAsInProgess()
        {
            return UpdateStatus(ResourceRequestStates.InProgress);
        }

        public bool SetAsFailed()
        {
            // Can raise an event to place on a dead letter queue 
            // Or let the Redis Pub/sub handle it
            return UpdateStatus(ResourceRequestStates.Failed);
            //throw new ApplicationException($"Batch {_batchResourceRequest.Id} failed");
        }

        bool UpdateStatus(ResourceRequestStates status)
        {
            _batchResourceRequest.SetState(status);
            return _database.StringSet(_batchResourceRequest.Id.ToString(), status.ToString());
        }

    }
}
