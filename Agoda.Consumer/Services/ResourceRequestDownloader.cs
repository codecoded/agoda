﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Agoda.Shared.Models;

namespace Agoda.Consumer.Services
{
    public class ResourceRequestDownloader
    {
        public ResourceRequest Request { get; private set; }

        public ResourceRequestDownloader(ResourceRequest request)
        {
            Request = request;
        }

        public async Task<bool> Download(string outputDir)
        {

            if (!Uri.IsWellFormedUriString(Request.Url, UriKind.RelativeOrAbsolute))
                return false;

            using (var client = CreateClient())
            {

                try
                {
                    Console.WriteLine("Downloading " + Request.Url);

                    using (var result = await client.GetAsync(Request.Url))
                    {
                        if (result.IsSuccessStatusCode)
                        {
                            var byteArray = await result.Content.ReadAsByteArrayAsync();

                            var process = System.Diagnostics.Process.GetCurrentProcess();
                            string filename = $"{outputDir}\\{Request.Filename}";

                            return ByteArrayToFile(filename, byteArray);
                        }
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool ByteArrayToFile(string filename, byte[] byteArray)
        {
            try
            {
                using (var fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    Console.WriteLine($"Saved contents to {filename}");

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }

        public HttpClient CreateClient()
        {
            // I've moved this to a methdd as the ResourceRequest may be extended
            // to include authorization headers etc so can refactor into a factory etc
            return new HttpClient();
        }
    }
}
