﻿using System;
using Agoda.Messaging;
using Agoda.Shared.Models;
using Confluent.Kafka;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Agoda.Consumer.Services
{
    public class MessageHandler
    {
        IDatabase _database;
        IMessageProducer _messageProducer;

        public MessageHandler(IDatabase database, IMessageProducer messageProducer)
        {
            _database = database;
            _messageProducer = messageProducer;
        }

        public void OnMessage(object sender, Message<string, string> msg)
        {
            Console.WriteLine($"Read '{msg.Key}' from: {msg.TopicPartitionOffset}");

            try
            {
                var request = JsonConvert.DeserializeObject<BatchResourceRequest>(msg.Value);

                // Important! 
                request.SetId(Guid.Parse(msg.Key));
                var batchRequestHandler = new BatchResourceRequestHandler(request, _database);

                batchRequestHandler.ProcessBatch();

                // I like this idea, but should be part of a notification / obserable chain
                // e,g, RaiseEvent BatchResourceRequestProcessed(request)
                // as this is tightly coupled to the workflow
                ForwardMessage(request);

                Console.WriteLine($"BatchRequestProcesed. Status {request.Status}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void ForwardMessage(BatchResourceRequest request)
        {
            if (request.Status != ResourceRequestStates.Completed && request.Status != ResourceRequestStates.Failed)
                return;

            var topic = $"batch_resource_request_{request.Status.ToString().ToLower()}";
            var response = _messageProducer.SendMessage(request, topic).Result;
            Console.WriteLine($"Forwarded {response.Id} to {topic} => {response.Status}");
        }

        //public void OnError(object sender, Message<string, string> msg);
        //public void OnConsumerMessage(object sender, Message<string, string> msg);

    }
}
