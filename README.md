##Agoda Resource Downloader


This repo uses docker containers for Kafka, Kakfa Manager and Redis, so to access them from an external application, you will need to set up a couple of linkes in your hosts file

```
127.0.0.1 rediscache
127.0.0.1 kafkaserver
```

*git clone https://codecoded@bitbucket.org/codecoded/agoda.git*

cd agode
(assuming 3 terminal windows. Can use Alt + Space on the root of solution to open Command Prompt quickly)

$ docker-compose up 

$ dotnet run --project .\Agoda.Consumer\Agoda.Consumer.csproj

$ dotnet run -p .\Agoda.Web\Agoda.Web.csproj

(or, VS solution, should be configured to run both the consumer and web on start so can run from there)

If all up and running, can post test request using (assuming host is running on 43527)

```
curl -X POST \
  http://localhost:43527/api/resources \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 65e68aa0-e913-b3aa-38bd-bf8cf341cc7b' \
  -d '{
  requests:[{
    url:"https://www.example.com",
    filename: "example.com.html"
  },
 {
   url:"http://www.pdf995.com/samples/pdf.pdf",
   filename: "vrml.pdf"
  },
  {
  	url:"http://ipv4.download.thinkbroadband.com/20MB.zip",
  	filename: "large_file.zip"
  },
  {
	url:"https://image.tmdb.org/t/p/w1280/jI1YUUasrswc73yCT6jekM5r8RY.jpg",
  	filename: "dinosaur_ROAR.jpg"
  }]
}
'
```

This should returna GUID

Can check process using 

```
curl -X GET \
  http://localhost:43527/api/resources/:message_id \
  -H 'cache-control: no-cache' '
```


Addendum:

I wanted to test with a real time update, so I wrote a little Agoda.Client console app. This is on the ClientApp branch. The reason I added this on a branch is that it changes the response of the WebAPI Post 
to use a Location header. This to me, is the correct practice. The request is accepted (202) and then a location Uri is returned for the client to know where to check. 

I use this location header to then check for the status of the request (the Get method)

Works really well...like a real messaging system ;)
