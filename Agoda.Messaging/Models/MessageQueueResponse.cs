﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Agoda.Messaging.Models
{
    public class MessageQueueResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; private set; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public MessageStates Status { get; set; }

        public MessageQueueResponse(Guid id)
        {
            Id = id;
        }
    }
}
