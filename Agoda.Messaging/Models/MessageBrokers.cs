﻿namespace Agoda.Messaging.Models
{
    public enum MessageBrokers
    {
        Kafka = 0,
        Redis,
        AzureEventHub
    }
}
