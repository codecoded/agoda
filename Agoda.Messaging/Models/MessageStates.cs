﻿namespace Agoda.Messaging.Models
{
    public enum MessageStates
    {
        Unknown = 0,
        Queued,
        InProgress,
        Completed,
        Failed
    }
}
