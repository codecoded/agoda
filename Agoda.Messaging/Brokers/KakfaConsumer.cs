﻿using System;
using System.Collections.Generic;
using System.Text;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;

namespace Agoda.Messaging.Brokers
{
    public class KafkaConsumer : IDisposable
    {
        public Consumer<string, string> Consumer;

        public KafkaConsumer(string brokerList)
        {
            Consumer = new Consumer<string, string>(GetDefaultConfig(brokerList),
                                                    new StringDeserializer(Encoding.UTF8),
                                                    new StringDeserializer(Encoding.UTF8));
        }

        public static Dictionary<string, object> GetDefaultConfig(string brokerList = "kafkaserver:9092")
        {
            return new Dictionary<string, object>
            {
                { "group.id", "resources-consumer-group" },
                { "bootstrap.servers", brokerList },
                { "auto.commit.interval.ms", 5000 },
                { "auto.offset.reset", "earliest" }
            };
        }

        public void ConsumeMessages(string topicName, double polltime = 100)
        {
            Consumer.Subscribe(topicName);
            while (true)
            {
                Consumer.Poll(TimeSpan.FromMilliseconds(polltime));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Consumer != null) Consumer.Dispose();
            }
        }
    }
}
