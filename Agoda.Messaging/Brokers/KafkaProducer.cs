﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Agoda.Messaging.Models;
using Agoda.Shared.Models;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Newtonsoft.Json;

namespace Agoda.Messaging.Brokers
{
    public class KafkaProducer : IMessageProducer, IDisposable
    {
        readonly Producer<string, string> Producer;

        public KafkaProducer(string brokerList)
        {
            Producer = new Producer<string, string>(GetDefaultConfig(brokerList),
                                                    new StringSerializer(Encoding.UTF8),
                                                    new StringSerializer(Encoding.UTF8));
        }

        public static Dictionary<string, object> GetDefaultConfig(string brokerList = "kafkaserver:9092")
        {
            return new Dictionary<string, object>
            {
                {
                    "bootstrap.servers", brokerList
                }
            };
        }

        public async Task<MessageQueueResponse> SendMessage(IIdentifiable request, string topicName)
        {
            var deliveryReport = await Producer.ProduceAsync(topicName, request.Id.ToString(), JsonConvert.SerializeObject(request));
            Producer.Flush(500);

            return new MessageQueueResponse(request.Id)
            {
                Status = deliveryReport.Error.HasError ? MessageStates.Failed : MessageStates.Queued
            };
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Producer != null) Producer.Dispose();
            }
        }

    }

}
