﻿using System.Threading.Tasks;
using Agoda.Messaging.Models;
using Agoda.Shared.Models;

namespace Agoda.Messaging
{
    public interface IMessageProducer
    {
        Task<MessageQueueResponse> SendMessage(IIdentifiable request, string topicName);

    }
}
