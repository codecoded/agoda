﻿using Agoda.Messaging.Brokers;
using Agoda.Messaging.Models;

namespace Agoda.Messaging
{
    public class MessageBrokerFactory
    {
        public static IMessageProducer GetBroker(MessageBrokers broker, string brokerList)
        {
            switch (broker)
            {
                case MessageBrokers.Kafka:
                default:
                    return new KafkaProducer(brokerList);
            }
        }
    }
}
