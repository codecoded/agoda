﻿using Newtonsoft.Json;

namespace Agoda.Shared.Models
{
    public class ResourceRequest
    {

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }

        [JsonProperty("state")]
        public ResourceRequestStates State { get; set; }

        // Other properties could be associated with this request
        // public int RetryCount {get;set;}
        // public Credentials {get;set;
    }
}
