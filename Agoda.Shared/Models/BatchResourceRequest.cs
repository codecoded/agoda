﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Agoda.Shared.Models
{
    public class BatchResourceRequest : IIdentifiable
    {
        [JsonProperty("requests")]
        public List<ResourceRequest> Requests { get; set; }

        [JsonIgnore]
        public ResourceRequestStates Status { get; private set; }


        public bool AllRequestsCompleted => Requests?.TrueForAll(e => e.State == ResourceRequestStates.Completed) ?? false;

        // Might want to handle a barch as a single unit of work
        // Can then be traced through the system
        public Guid Id { get; private set; }

        public BatchResourceRequest()
        {
            Id = Guid.NewGuid();
        }

        public void SetId(Guid id)
        {
            Id = id;
        }

        public void SetState(ResourceRequestStates state)
        {
            switch (state)
            {
                case ResourceRequestStates.InProgress:
                    if (Status == ResourceRequestStates.Queued)
                        Status = state;
                    else
                        throw new InvalidOperationException($"Cannot go to in InProgress state from ${Status}");
                    break;
                case ResourceRequestStates.Completed:
                    if (Status == ResourceRequestStates.InProgress)
                        Status = state;
                    else
                        throw new InvalidOperationException($"Cannot go to in Completed state from ${Status}");
                    break;
                default:
                    Status = state;
                    break;
            }
        }

    }
}
