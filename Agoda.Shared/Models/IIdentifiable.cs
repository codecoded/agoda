﻿using System;
namespace Agoda.Shared.Models
{
    public interface IIdentifiable
    {
        Guid Id { get; }
    }
}
