﻿namespace Agoda.Shared.Models
{
    public enum ResourceRequestStates
    {
        Unknown = 0,
        Queued,
        InProgress,
        Completed,
        Failed
    }
}
